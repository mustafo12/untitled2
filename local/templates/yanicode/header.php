<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}



/** @global $APPLICATION */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/script.js');

Asset::getInstance()->addString('<link rel="icon" type="image/x-icon" href="' . SITE_TEMPLATE_PATH . '/favicon.ico"/>');
Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');
?>
<!doctype html>
<html xml:lang="<?php echo LANGUAGE_ID ?>" lang="<?php echo LANGUAGE_ID ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta class="js-meta-viewport" name="viewport" content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="/local/templates/yanicode/assets/template_styles.css">

<?php $APPLICATION->ShowHead(); ?>
</head>
<body>
<div id="panel"><?php $APPLICATION->ShowPanel() ?></div>
<header class="header">
    <div class="container">
        <div class="header-wrapper">
            <a href="../../../onas.php" class="header__logo">
                <img width="300" height="92" src="/local/templates/yanicode/assets/images/svg/logo-yanicode.svg" alt="yanicode">
            </a>
                <div>
                    <div class="header__burger header__burger_close">
                        <span class="burger-line"></span>
                        <span class="burger-line"></span>
                        <span class="burger-line"></span>
                    </div>
                    <div class="header-nav">
                        <div class="header-nav">


                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "yanicode",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "top",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "top",
                                        "USE_EXT" => "N"
                                    )
                                );?>

                            <div class="header__phone">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "yanicode",
                                    Array(
                                        "AREA_FILE_SHOW" => "page",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "COMPONENT_TEMPLATE" => "yanicode",
                                        "EDIT_TEMPLATE" => ""
                                    )
                                );?>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</header>

</body>
</html>
