<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$themeClass = isset($arParams['TEMPLATE_THEME']) ? ' bx-'.$arParams['TEMPLATE_THEME'] : '';
?>

<section class="container">
    <div class="blog-list">
        <?if($arParams["DISPLAY_TOP_PAGER"]):?>
            <?=$arResult["NAV_STRING"]?><br />
        <?endif;?>


            <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction(
                $arItem['ID'],
                $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID(
                    $arItem["IBLOCK_ID"],
                    "ELEMENT_EDIT"
                )
            );
            $this->AddDeleteAction(
                $arItem['ID'],
                $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID(
                    $arItem["IBLOCK_ID"],
                    "ELEMENT_DELETE"),
                array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
            );
            ?>
            <div class="" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="blog">
                    <div class="blog">
                        <?if($arParams["DISPLAY_PICTURE"]!="N"):?>
                            <?
                            if ($arItem["VIDEO"])
                            {
                                ?>
                            <?
                            }
                            else if ($arItem["SOUND_CLOUD"])
                            {
                            ?>
                            <?
                            }
                            else if ($arItem["SLIDER"] && count($arItem["SLIDER"]) > 1)
                            {
                            ?>
                                <div class="news-list-item-embed-slider">
                                    <div class="news-list-slider-container" style="width: <?
                                    echo count($arItem["SLIDER"]) * 100 ?>%;left: 0;">
                                        <?
                                        foreach ($arItem["SLIDER"] as $file):?>
                                            <div class="blog__img">
                                                <img width="100%" height="100%" src="<?= $file["SRC"] ?>" alt="<?= $file["DESCRIPTION"] ?>">
                                            </div>
                                        <?endforeach ?>
                                    </div>
                                    <div class="news-list-slider-arrow-container-left">
                                        <div class="news-list-slider-arrow"><i class="fa fa-angle-left"></i></div>
                                    </div>
                                    <div class="news-list-slider-arrow-container-right">
                                        <div class="news-list-slider-arrow"><i class="fa fa-angle-right"></i></div>
                                    </div>
                                    <ul class="news-list-slider-control">
                                        <?
                                        foreach ($arItem["SLIDER"] as $i => $file):?>
                                            <li rel="<?= ($i + 1) ?>" <?
                                            if (!$i)
                                                echo 'class="current"' ?>><span></span></li>
                                        <?endforeach ?>
                                    </ul>
                                </div>

                            <?
                            }
                            else if ($arItem["SLIDER"])
                            {
                            ?>
                                <div class="news-list-item-embed-img">
                                    <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"]))
                                    {
                                        ?>

                                        <img
                                                class="blog__img"
                                                src="<?= $arItem["SLIDER"][0]["SRC"] ?>"
                                                width="<?= $arItem["SLIDER"][0]["WIDTH"] ?>"
                                                height="<?= $arItem["SLIDER"][0]["HEIGHT"] ?>"
                                                alt="<?= $arItem["SLIDER"][0]["ALT"] ?>"
                                                title="<?= $arItem["SLIDER"][0]["TITLE"] ?>"
                                        />

                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <img
                                                class="blog__img"
                                                src="<?= $arItem["SLIDER"][0]["SRC"] ?>"
                                                width="<?= $arItem["SLIDER"][0]["WIDTH"] ?>"
                                                height="<?= $arItem["SLIDER"][0]["HEIGHT"] ?>"
                                                alt="<?= $arItem["SLIDER"][0]["ALT"] ?>"
                                                title="<?= $arItem["SLIDER"][0]["TITLE"] ?>"
                                        />
                                        <?
                                    }
                                    ?>
                                </div>
                                <?
                            }
                            else if (is_array($arItem["PREVIEW_PICTURE"]))
                            {
                            if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"]))
                            {
                                ?>

                            <img
                                    class="blog__img"
                                    src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                    alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                    title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                            />
                                <?
                            }
                            else
                            {
                                ?>
                            <img
                                    src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                    class="blog__img"
                                    alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                    title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                            />
                                <?
                            }
                            }
                            ?>

                        <?endif;?>

                        <div class="blog__desc">

                            <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
                                <div class="blog__title">
                                    <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                                        <?echo $arItem["NAME"]?>
                                    <?else:?>
                                        <?echo $arItem["NAME"]?>
                                    <?endif;?>
                                </div>
                            <?endif;?>

                            <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                                <?
                                if(is_array($arProperty["DISPLAY_VALUE"]))
                                    $value = implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
                                else
                                    $value = $arProperty["DISPLAY_VALUE"];
                                ?>
                                <?if($arProperty["CODE"] == "FORUM_MESSAGE_CNT"):?>
                                    <div class="news-list-view news-list-post-params">
                                        <span class="news-list-icon news-list-icon-comments"></span>
                                        <span class="blog__date"><?=$arProperty["NAME"]?>:<?=$value;?></span>
                                        <span class="news-list-value"><?=$value;?></span>
                                    </div>
                                <?elseif ($value != ""):?>
                                    <div class="news-list-view news-list-post-params">
                                        <span class="news-list-icon"></span>
                                        <span class="blog__date"><?=$arProperty["NAME"]?>:</span>
                                        <span class="news-list-value"><?=$value;?></span>
                                    </div>
                                <?endif;?>
                            <?endforeach;?>
                            <div class="blog__date">

                                <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                                    <div class="blog__date">

                                        <span class="blog__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
                                    </div>
                                <?endif?>

                                <?if($arParams["USE_RATING"]=="Y"):?>
                                    <div>
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:iblock.vote",
                                            "bootstrap_v4",
                                            Array(
                                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                                "ELEMENT_ID" => $arItem["ID"],
                                                "MAX_VOTE" => $arParams["MAX_VOTE"],
                                                "VOTE_NAMES" => $arParams["VOTE_NAMES"],
                                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                                "DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"],
                                                "SHOW_RATING" => "N",
                                            ),
                                            $component
                                        );?>
                                    </div>
                                <?endif?>
                            </div>

                            <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                            <div class="blog_article"><?echo $arItem["PREVIEW_TEXT"];?></div>
                        </div>
                    <?endif;?>
                        <?foreach($arItem["FIELDS"] as $code=>$value):?>
                            <?if($code == "SHOW_COUNTER"):?>
                                <div class="news-list-view news-list-post-params">
                                    <span class="news-list-icon news-list-icon-eye"></span>
                                    <span class="news-list-param"><?=GetMessage("IBLOCK_FIELD_".$code)?>: </span>
                                    <span class="news-list-value"><?=intval($value);?></span>
                                </div>
                            <?elseif(
                                $value
                                && (
                                    $code == "SHOW_COUNTER_START"
                                    || $code == "DATE_ACTIVE_FROM"
                                    || $code == "ACTIVE_FROM"
                                    || $code == "DATE_ACTIVE_TO"
                                    || $code == "ACTIVE_TO"
                                    || $code == "DATE_CREATE"
                                    || $code == "TIMESTAMP_X"
                                )
                            ):?>
                                <?
                                $value = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($value, CSite::GetDateFormat()));
                                ?>
                                <div class="news-list-view news-list-post-params">
                                    <span class="news-list-icon news-list-icon-calendar"></span>
                                    <span class="news-list-param"><?=GetMessage("IBLOCK_FIELD_".$code)?>: </span>
                                    <span class="news-list-value"><?=$value;?></span>
                                </div>
                            <?elseif($code == "TAGS" && $value):?>
                                <div class="news-list-view news-list-post-params">
                                    <span class="news-list-icon news-list-icon-tag"></span>
                                    <span class="news-list-param"><?=GetMessage("IBLOCK_FIELD_".$code)?>:</span>
                                    <span class="news-list-value"><?=$value;?></span>
                                </div>
                            <?elseif(
                                $value
                                && (
                                    $code == "CREATED_USER_NAME"
                                    || $code == "USER_NAME"
                                )
                            ):?>
                                <div class="news-list-view news-list-post-params">
                                    <span class="news-list-icon news-list-icon-user"></span>
                                    <span class="news-list-param"><?=GetMessage("IBLOCK_FIELD_".$code)?>:</span>
                                    <span class="news-list-value"><?=$value;?></span>
                                </div>
                            <?elseif ($value != ""):?>
                                <div class="news-list-view news-list-post-params">
                                    <span class="news-list-icon"></span>
                                    <span class="news-list-param"><?=GetMessage("IBLOCK_FIELD_".$code)?>:</span>
                                    <span class="news-list-value"><?=$value;?></span>
                                </div>
                            <?endif;?>
                        <?endforeach;?>


                        <div class="d-flex justify-content-between align-items-center">
                            <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                                <div class="news-list-more">

                                </div>
                            <?endif;?>
                            <? if ($arParams["USE_SHARE"] == "Y")
                            {
                                ?>
                                <div class="text-right">
                                    <?
                                    $APPLICATION->IncludeComponent("bitrix:main.share", $arParams["SHARE_TEMPLATE"], array(
                                        "HANDLERS" => $arParams["SHARE_HANDLERS"],
                                        "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                                        "PAGE_TITLE" => $arResult["~NAME"],
                                        "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                                        "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                                        "HIDE" => $arParams["SHARE_HIDE"],
                                    ),
                                        $component,
                                        array("HIDE_ICONS" => "Y")
                                    );
                                    ?>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                </a>
            </div>


        <?endforeach;?>
    </div>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
</section>