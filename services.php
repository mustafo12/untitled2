<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$GLOBALS["arrFilterMainTheme"] = array("PROPERTY_MAIN_VALUE" => 1);
$GLOBALS["arrFilterMain"] = array("PROPERTY_MAIN_VALUE" => 1);
?>
<head>
    <title>Услуги</title>
</head>
<main class="website-workarea">
    <section class="banner banner_before54" style="background-image: url('local/templates/yanicode/assets/images/banner_services.jpg');">
        <div class="banner-wrapper">
            <div class="container">
                <div class="banner__content">
                    <p>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "ser",
                            Array(
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc",
                                "COMPONENT_TEMPLATE" => "ser",
                                "EDIT_TEMPLATE" => ""
                            )
                        );?>
                    </p>
                </div>
                <br>
            </div>
        </div>

        <div class="container">
            <div class="stages">
                <div class="stages__item">
                    <div class="stages__step">01</div>
                    <div class="stages__desc-step">АУДИТ</div>
                </div>
                <div class="stages__item">
                    <div class="stages__step">02</div>
                    <div class="stages__desc-step">СТРАТЕГИЯ</div>
                </div>
                <div class="stages__item">
                    <div class="stages__step">03</div>
                    <div class="stages__desc-step">КОНЦЕПЦИЯ</div>
                </div>
                <div class="stages__item">
                    <div class="stages__step">04</div>
                    <div class="stages__desc-step">ДИЗАЙН</div>
                </div>
                <div class="stages__item">
                    <div class="stages__step">05</div>
                    <div class="stages__desc-step">КОММУНИКАЦИИ</div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <h1 class="container-title">
            НАШИ УСЛУГИ            </h1>
        <div class="services-cover">
            <div class="services">
                <h2 class="services__title">БРЕНДИНГ</h2>
                <div class="services-category">
                    <div class="services__item">Коммуникационная стратегия</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <a class="services__item" data-id="432" data-popup="services-popup">Внедрение</a>
                    <a class="services__item" data-id="431" data-popup="services-popup">Digital Branding</a>
                    <a class="services__item" data-id="430" data-popup="services-popup">Дизайн упаковки</a>
                    <a class="services__item" data-id="429" data-popup="services-popup">Дизайн-концепт упаковки</a>
                    <a class="services__item" data-id="428" data-popup="services-popup">Бренд-бук</a>
                    <a class="services__item" data-id="427" data-popup="services-popup">Фирменный стиль</a>
                    <a class="services__item" data-id="426" data-popup="services-popup">Регистрация товарного знака</a>
                    <a class="services__item" data-id="425" data-popup="services-popup">Логотип</a>
                    <a class="services__item" data-id="424" data-popup="services-popup">Нейминг</a>
                    <a class="services__item" data-id="423" data-popup="services-popup">Концепция/Легенда</a>
                    <a class="services__item" data-id="422" data-popup="services-popup">Позиционирование</a>
                    <a class="services__item" data-id="421" data-popup="services-popup">Целевая аудитория</a>
                    <a class="services__item" data-id="420" data-popup="services-popup">Аудит бренда / cитуации</a>
                </div>
            </div>
            <div class="services">
                <h2 class="services__title">МАССОВЫЕ КОММУНИКАЦИИ</h2>
                <div class="services-category">
                    <a class="services__item" data-id="504" data-popup="services-popup">Оценка эффективности</a>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <a class="services__item" data-id="443" data-popup="services-popup">Сопровождение</a>
                    <a class="services__item" data-id="442" data-popup="services-popup">Внедрение</a>
                    <a class="services__item" data-id="441" data-popup="services-popup">Дизайн</a>
                    <a class="services__item" data-id="440" data-popup="services-popup">Фотопродакшн</a>
                    <a class="services__item" data-id="439" data-popup="services-popup">Видеопродакшн</a>
                    <a class="services__item" data-id="438" data-popup="services-popup">Медиапланирование</a>
                    <a class="services__item" data-id="437" data-popup="services-popup">Тактические рекламные кампании</a>
                    <a class="services__item" data-id="436" data-popup="services-popup">Креативный концепт</a>
                    <a class="services__item" data-id="435" data-popup="services-popup">Коммуникационная стратегия</a>
                    <a class="services__item" data-id="434" data-popup="services-popup">Целевая аудитория</a>
                    <a class="services__item" data-id="433" data-popup="services-popup">Аудит коммуникаций бренда</a>
                </div>
            </div>
            <div class="services">
                <h2 class="services__title">DIGITAL БРЕНДИНГ</h2>
                <div class="services-category">
                    <a class="services__item" data-id="453" data-popup="services-popup">Коммуникационная стратегия</a>
                    <a class="services__item" data-id="452" data-popup="services-popup">Сайт</a>
                    <a class="services__item" data-id="451" data-popup="services-popup">Контент-план</a>
                    <a class="services__item" data-id="450" data-popup="services-popup">Оформление соц сетей</a>
                    <a class="services__item" data-id="449" data-popup="services-popup">Видеовизитка</a>
                    <a class="services__item" data-id="448" data-popup="services-popup">Фотосессия</a>
                    <a class="services__item" data-id="447" data-popup="services-popup">Разработка уникального контента</a>
                    <a class="services__item" data-id="446" data-popup="services-popup">Фирменный стиль</a>
                    <a class="services__item" data-id="445" data-popup="services-popup">Позиционирование</a>
                    <a class="services__item" data-id="444" data-popup="services-popup">Аудит ситуации</a>
                </div>
            </div>
            <div class="services">
                <h2 class="services__title">ЛИЧНЫЙ БРЕНД</h2>
                <div class="services-category">
                    <a class="services__item" data-id="528" data-popup="services-popup">Упаковка продукта</a>
                    <a class="services__item" data-id="463" data-popup="services-popup">Сайт</a>
                    <a class="services__item" data-id="462" data-popup="services-popup">Подбор специалистов</a>
                    <a class="services__item" data-id="461" data-popup="services-popup">Фотосессия</a>
                    <a class="services__item" data-id="460" data-popup="services-popup">Контент-план</a>
                    <a class="services__item" data-id="459" data-popup="services-popup">Оформление соцсетей</a>
                    <a class="services__item" data-id="458" data-popup="services-popup">Стратегия продвижения</a>
                    <a class="services__item" data-id="457" data-popup="services-popup">Личная легенда</a>
                    <a class="services__item" data-id="456" data-popup="services-popup">Позиционирование</a>
                    <a class="services__item" data-id="455" data-popup="services-popup">Целевая аудитория</a>
                    <a class="services__item" data-id="454" data-popup="services-popup">Аудит ситуации</a>
                </div>
            </div>

        </div>

        <div class="button-wrapper  button-wrapper_center">
            <div class="button button_gold button-open-calculate-project" data-popup="calculate-project-popup">
                РАССЧИТАТЬ ПРОЕКТ
            </div>
            <div class="button button_gold button-open-feedback" data-popup="feedback-popup">
                ЗАПИСАТЬСЯ НА ОНЛАЙН ВСТРЕЧУ
                </div>
        </div>
    </div>

    <template id="popup-services">
        <div class="popup-services-cover">
            <div class="popup-services__title">
                БРЕНДИНГ
            </div>
            <div class="popup-services__category">
                Аудит бренда
            </div>
            <div class="popup-services__desc">
                <p>
                    Это диагностика его ключевых параметров и характеристик для понимания дальнейших путей развития. Прежде всего, он дает ответ на вопрос: что сохранить и что изменить при ребрендинге.
                </p>
                <p>
                    В базовой версии аудита мы анализируем восприятие бренда; целостность его образа в различных точках контакта с потребителем; заметность бренда в конкурентном окружении. Особое внимание мы уделяем анализу фирменного стиля и коммуникаций, поскольку это именно та сфера, в которой нам предстоит работать. По итогам аудита мы даем рекомендации по возможным направлениям развития фирменного стиля бренда.
                </p>
                <p>
                    Если на момент проведения аудита маркетинговая стратегия уже сформирована, мы сопоставляем цели и планы компании с текущим образом бренда.
                </p>
                <p>
                    В расширенных версиях может быть проведен анализ ассортиментного портфеля, пути потребителя, представленности в тех или иных каналах продаж и коммуникаций и т.д. Набор параметров для анализа определяется индивидуально для каждого проекта.
                </p>
            </div>
        </div>
    </template>
    <template id="popup-calculate-project">
        <div class="js-validated-form">
            <?$APPLICATION->IncludeComponent(
                "bitrix:form.result.new",
                ".default",
                Array(
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "CHAIN_ITEM_LINK" => "",
                    "CHAIN_ITEM_TEXT" => "",
                    "EDIT_ADDITIONAL" => "N",
                    "EDIT_STATUS" => "Y",
                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                    "NAME_TEMPLATE" => "",
                    "NOT_SHOW_FILTER" => array("",""),
                    "NOT_SHOW_TABLE" => array("",""),
                    "RESULT_ID" => $_REQUEST[RESULT_ID],
                    "SEF_MODE" => "N",
                    "SHOW_ADDITIONAL" => "N",
                    "SHOW_ANSWER_VALUE" => "N",
                    "SHOW_EDIT_PAGE" => "N",
                    "SHOW_LIST_PAGE" => "N",
                    "SHOW_STATUS" => "Y",
                    "SHOW_VIEW_PAGE" => "Y",
                    "START_PAGE" => "new",
                    "SUCCESS_URL" => "",
                    "USE_EXTENDED_ERRORS" => "N",
                    "VARIABLE_ALIASES" => Array("action"=>"action"),
                    "WEB_FORM_ID" => "3"
                )
            );?><br>
        </div>
    </template>

    <template id="popup-feedback">
        <form action="/services/" method="post" enctype="multipart/form-data" class="js-validated-form">
            <h2 class="result"></h2>
            <div id="comp_3bcb123c6f0318b32c45337bcf616f42">
                <div class="popup-feedback__input-cover">
                    <label for="" class="popup-feedback__input-label">Ваше имя</label>
                    <input type="text" name="name" class="popup-feedback__input js-validated-field" data-validated_name="name">
                </div>
                <div class="popup-feedback__double-column">
                    <div class="popup-feedback__input-cover">
                        <label for="" class="popup-feedback__input-label">Телефон</label>
                        <input type="tel" class="popup-feedback__input mask-phone-js js-validated-field" data-validated_name="phone" name="phone">
                    </div>
                    <div class="popup-feedback__input-cover">
                        <label for="" class="popup-feedback__input-label">Email</label>
                        <input type="mail" name="mail" class="popup-feedback__input js-validated-field" data-validated_name="mail">
                    </div>
                </div>
                <div class="communication-format-text">
                    Выберите удобный формат связи::
                </div>
                <div class="communication-format">
                    <div class="communication-format__item" title="whatsapp">
                        <!--                value = "34" для сохранения элементов списка в инфоблоке dev, value = "50" - prod (whatsapp)-->
                        <input class="communication-format__checkbox" id="feedback-whatsapp" type="checkbox" name="way1" value="50">
                        <label class="communication-format__label" for="feedback-whatsapp">
                            <svg width="49" height="51" viewbox="0 0 49 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path class="communication-format__svg" d="M24.5061 0H24.4939C10.9852 0 0 11.2168 0 25.0096C0 30.4805 1.72725 35.5512 4.66419 39.6684L1.61088 48.9594L11.0281 45.8864C14.9021 48.5061 19.5234 50.0192 24.5061 50.0192C38.0148 50.0192 49 38.7993 49 25.0096C49 11.2199 38.0148 0 24.5061 0ZM38.7621 35.3167C38.171 37.0205 35.8251 38.4335 33.9539 38.8462C32.6738 39.1244 31.0017 39.3464 25.3728 36.9642C18.1729 33.9193 13.5363 26.4508 13.1749 25.9662C12.8288 25.4817 10.2655 22.0116 10.2655 18.4227C10.2655 14.8338 12.0509 13.0863 12.7706 12.336C13.3617 11.7201 14.3386 11.4388 15.2758 11.4388C15.5789 11.4388 15.8515 11.4544 16.0965 11.4669C16.8162 11.4982 17.1776 11.5419 17.6523 12.7018C18.2433 14.1554 19.6827 17.7443 19.8542 18.1132C20.0287 18.4821 20.2033 18.9823 19.9583 19.4669C19.7286 19.9671 19.5265 20.189 19.1651 20.6142C18.8037 21.0393 18.4608 21.3645 18.0994 21.8209C17.7686 22.2179 17.395 22.6431 17.8115 23.3777C18.228 24.0968 19.6674 26.4946 21.7866 28.4203C24.5214 30.9056 26.7387 31.6997 27.5319 32.0373C28.1229 32.2874 28.8273 32.228 29.2591 31.7591C29.8073 31.1557 30.4841 30.1553 31.1732 29.1706C31.6632 28.4641 32.2818 28.3765 32.9311 28.6266C33.5926 28.8611 37.093 30.6274 37.8127 30.9932C38.5324 31.3621 39.0071 31.5371 39.1816 31.8466C39.3531 32.1561 39.3531 33.6098 38.7621 35.3167Z" fill="white"></path>
                            </svg>
                        </label>
                    </div>
                    <div class="communication-format__item" title="skype">
                        <!--                value = "35" для сохранения элементов списка в инфоблоке dev, value = "51" - prod (skype)-->
                        <input class="communication-format__checkbox" id="feedback-skype" type="checkbox" name="way2" value="51">
                        <label class="communication-format__label" for="feedback-skype">
                            <svg width="49" height="51" viewbox="0 0 49 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path class="communication-format__svg" d="M47.5419 30.2886C51.0868 14.0299 37.1316 -0.740436 20.7431 2.09496C11.8462 -3.54044 0 2.82775 0 13.6322C0 16.1449 0.6792 18.4974 1.86015 20.525C-1.43387 36.8213 12.6356 51.3792 29.0098 48.3502C40.5847 54.5977 53.3753 42.1382 47.5419 30.2886ZM31.9632 40.0938C27.6636 41.8987 20.5881 41.9092 16.3069 39.6213C10.2023 36.2987 9.11718 28.8189 14.3835 28.8189C18.3527 28.8189 17.0962 33.5425 21.1041 35.4681C22.9439 36.3362 26.8988 36.4257 29.1974 34.8436C31.4675 33.2926 31.2595 30.8528 30.0255 29.687C26.756 26.6059 17.4002 27.83 13.123 22.4361C11.2649 20.0962 10.9141 15.968 13.1985 12.9744C17.1799 7.74493 28.8629 7.42642 33.8866 11.3756C38.5247 15.0395 37.2724 19.9026 33.5296 19.9026C29.0281 19.9026 31.3941 13.8904 24.1269 13.8904C18.8585 13.8904 16.7862 17.7146 20.4963 19.5757C25.5261 22.1259 38.3432 21.2745 38.3432 31.24C38.333 35.3869 35.8426 38.4784 31.9632 40.0938Z" fill="white"></path>
                            </svg>
                        </label>
                    </div>
                    <div class="communication-format__item" title="zoom">
                        <!--value = "36" для сохранения элементов списка в инфоблоке dev, value = "52" - prod (zoom)-->
                        <input class="communication-format__checkbox" id="feedback-zoom" type="checkbox" name="way3" value="52">
                        <label class="communication-format__label" for="feedback-zoom">
                            <svg width="49" height="51" viewbox="0 0 49 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path class="communication-format__svg" d="M24.5 0.980835C10.9693 0.980835 0 12.1783 0 25.9904C0 39.8025 10.9693 51 24.5 51C38.0307 51 49 39.8025 49 25.9904C49 12.1783 38.0307 0.980835 24.5 0.980835ZM30.2625 33.0569C30.261 33.2658 30.1784 33.4655 30.0329 33.6123C29.8873 33.7591 29.6907 33.841 29.4861 33.8399H14.3501C13.224 33.8443 12.1422 33.3921 11.3423 32.5828C10.5425 31.7735 10.0901 30.6732 10.0844 29.5237V18.9239C10.0859 18.7151 10.1684 18.5153 10.314 18.3686C10.4596 18.2218 10.6562 18.1399 10.8608 18.1409H25.9968C27.1229 18.1366 28.2047 18.5887 29.0045 19.398C29.8044 20.2073 30.2568 21.3076 30.2625 22.4572V33.0569ZM38.4385 33.0951C38.4385 34.0405 37.924 33.9259 37.475 33.5821L31.226 28.9316V23.0588L37.475 18.3987C38.0176 17.9404 38.4385 18.055 38.4385 18.8858V33.0951Z" fill="white"></path>
                            </svg>
                        </label>
                    </div>
                    <div class="communication-format__item" title="viber">
                        <!--                value = "37" для сохранения элементов списка в инфоблоке dev, value = "53" - prod (viber)-->
                        <input class="communication-format__checkbox" id="feedback-viber" type="checkbox" name="way4" value="53">
                        <label class="communication-format__label" for="feedback-viber">
                            <svg width="49" height="51" viewbox="0 0 49 51" xmlns="http://www.w3.org/2000/svg">
                                <path class="communication-format__svg" d="M24.5061 0H24.4939C10.9852 0 0 11.2168 0 25.0096C0 30.4805 1.72725 35.5512 4.66419 39.6684L1.61088 48.9594L11.0281 45.8864C14.9021 48.5061 19.5234 50.0192 24.5061 50.0192C38.0148 50.0192 49 38.7993 49 25.0096C49 11.2199 38.0148 0 24.5061 0ZM38.7621 35.3167C38.171 37.0205 35.8251 38.4335 33.9539 38.8462C32.6738 39.1244 31.0017 39.3464 25.3728 36.9642C18.1729 33.9193 13.5363 26.4508 13.1749 25.9662C12.8288 25.4817 10.2655 22.0116 10.2655 18.4227C10.2655 14.8338 12.0509 13.0863 12.7706 12.336C13.3617 11.7201 14.3386 11.4388 15.2758 11.4388C15.5789 11.4388 15.8515 11.4544 16.0965 11.4669C16.8162 11.4982 17.1776 11.5419 17.6523 12.7018C18.2433 14.1554 19.6827 17.7443 19.8542 18.1132C20.0287 18.4821 20.2033 18.9823 19.9583 19.4669C19.7286 19.9671 19.5265 20.189 19.1651 20.6142C18.8037 21.0393 18.4608 21.3645 18.0994 21.8209C17.7686 22.2179 17.395 22.6431 17.8115 23.3777C18.228 24.0968 19.6674 26.4946 21.7866 28.4203C24.5214 30.9056 26.7387 31.6997 27.5319 32.0373C28.1229 32.2874 28.8273 32.228 29.2591 31.7591C29.8073 31.1557 30.4841 30.1553 31.1732 29.1706C31.6632 28.4641 32.2818 28.3765 32.9311 28.6266C33.5926 28.8611 37.093 30.6274 37.8127 30.9932C38.5324 31.3621 39.0071 31.5371 39.1816 31.8466C39.3531 32.1561 39.3531 33.6098 38.7621 35.3167Z" fill="white"></path>
                            </svg>
                        </label>
                    </div>
                </div>
                <div class="popup-feedback__consent">
                    <div class="popup-feedback__consent-form-wrapper">
                        <input class="popup-feedback__consent-input" id="consent1" type="checkbox" checked="checked">
                        <label class="popup-feedback__consent-form" for="consent1">
                            <a href="../data-processing/index.htm" target="_blank">
                                Нажимая кнопку «Отправить», я даю свое согласие на обработку моих персональных данных, в соответствии с Федеральным законом от 27.07.2006 года №152-ФЗ «О персональных данных», на условиях и для целей, определенных в Согласии на обработку персональных данных.            </a>
                        </label>
                    </div>
                    <div class="popup-feedback__consent-form-wrapper">
                        <input class="popup-feedback__consent-input" id="consent2" type="checkbox" checked="checked">
                        <label class="popup-feedback__consent-form" for="consent2">
                            <a href="../privacy-policy/index.htm" target="_blank">
                                Оставляя данные на Сайте, заполняя регистрационную форму, Вы соглашаетесь с настоящей Политикой конфиденциальности.            </a>
                        </label>
                    </div>
                </div>

                <div class="popup-feedback__button-cover">
                    <button type="submit" class="button button_modal-gold js-button-submit"> отправить</button>
                </div>
            </div>    </form>
    </template>

</main>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");?>

