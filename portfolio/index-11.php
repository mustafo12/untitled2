﻿<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$GLOBALS["arrFilterMainTheme"] = array("PROPERTY_MAIN_VALUE" => 1);
$GLOBALS["arrFilterMain"] = array("PROPERTY_MAIN_VALUE" => 1);
?>
<!doctype html>
<html xml:lang="ru" lang="ru">
<head>
    <title>Брендинг и массовые коммуникации брендов от агентства YANICODE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="index, follow">
<meta name="description" content="Брендинг и массовые коммуникации брендов от агентства YANICODE">
<script>var _ba = _ba || []; _ba.push(["aid", "2de29308e29493b74e79abe2c8129afd"]);(function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = document.location.protocol + "//bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>
<script type="text/javascript">if(!window.BX)window.BX={message:function(mess){if(typeof mess=='object') for(var i in mess) BX.message[i]=mess[i]; return true;}};</script>
<script type="text/javascript">(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','SERVER_TZ_OFFSET':'10800','SITE_ID':'s2','USER_ID':'','SERVER_TIME':'1647505457','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'2e81888949525f76737e1555de427c7f'});</script>



<script type="text/javascript">
bxSession.Expand(1440, '2e81888949525f76737e1555de427c7f', false, 'c26eccfe7fcc0bd62d50f5413b639369');
</script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div id="panel">

</div>
    <main class="website-workarea">
        <section class="banner banner_before49" style=" background-image: url('/local/templates/yanicode/assets/images/banner_portfolio.jpg');">
            <div class="banner-wrapper">
                <div class="container">
                    <div class="banner__content">
                        <p>
                            Совершаем переделы рынка мирным<br>
путем инструментами брендинга от<br>
создания до продвижения на рынке.<br>
Проверить прецедентом возможно                        </p>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="stages">
                    <div class="stages__item">
                        <div class="stages__step">01</div>
                        <div class="stages__desc-step">АУДИТ</div>
                    </div>
                    <div class="stages__item">
                        <div class="stages__step">02</div>
                        <div class="stages__desc-step">СТРАТЕГИЯ</div>
                    </div>
                    <div class="stages__item">
                        <div class="stages__step">03</div>
                        <div class="stages__desc-step">КОНЦЕПЦИЯ</div>
                    </div>
                    <div class="stages__item">
                        <div class="stages__step">04</div>
                        <div class="stages__desc-step">ДИЗАЙН</div>
                    </div>
                    <div class="stages__item">
                        <div class="stages__step">05</div>
                        <div class="stages__desc-step">КОММУНИКАЦИИ</div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <h1 class="container-title" id="portfolio">
                    НАШЕ ПОРТФОЛИО                </h1>

                <div class="portfolio-filter">
                    <div class="portfolio-filter__line portfolio-filter__line_small">
                        <a class="portfolio-filter__item" href="index-1.php">Персональный брендинг</a>
                        <a class="portfolio-filter__item" href="index-2.php">Корпоративный брендинг</a>
                        <a class="portfolio-filter__item" href="index-3.php">Потребительский брендинг</a>
                        <a class="portfolio-filter__item" href="index-4.php">Ресторанный брендинг</a>
                    </div>
                    <div class="portfolio-filter__line portfolio-filter__line_middle">
                        <a class="portfolio-filter__item" href="index-5.php">Нейминг</a>
                        <a class="portfolio-filter__item" href="index-6.php">Логотип</a>
                        <a class="portfolio-filter__item" href="index-7.php">Упаковка</a>
                        <a class="portfolio-filter__item" href="index-8.php">Фирменный стиль</a>
                        <a class="portfolio-filter__item" href="index-9.php">Брендбук</a>
                    </div>
                    <div class="portfolio-filter__line portfolio-filter__line_big">
                        <a class="portfolio-filter__item" href="index-10.php">Стратегии</a>
                        <a class="portfolio-filter__item" href="index-11.php">Рекламные кампании</a>
                        <a class="portfolio-filter__item" href="index-12.php">Digital branding</a>
            </div>
                </div>
            </div>
            <div class="portfolio-list">
                                

<br>
            <a href="495/index.php" class="portfolio-list__item" style="background-image: url('/local/templates/yanicode/assets/upload/iblock/291/Wheel.jpg');">
        <span class="portfolio-list__title">КУРС НА БЕЗОПАСНОСТЬ!</span>
        <span class="portfolio-list__desc">Позиционирующая креативная концепция и&nbsp;&nbsp;рекламная кампания </span>

            <a href="493/index.php" class="portfolio-list__item" style="background-image: url('/local/templates/yanicode/assets/upload/iblock/045/Anons rgb.jpg');">
        <span class="portfolio-list__title">GIANT. ВМЕСТЕ ПО ЖИЗНИ!</span>
        <span class="portfolio-list__desc">Креативный концепт и интегрированная рекламная кампания</span>

            <a href="492/index.php" class="portfolio-list__item" style="background-image: url('/local/templates/yanicode/assets/upload/iblock/01a/ETB TV Man.jpg');">
        <span class="portfolio-list__title">ВСЕ ВОЗМОЖНО!</span>
        <span class="portfolio-list__desc">Креативный концепт и интегрированная рекламная кампания </span>

            <a href="491/index.php" class="portfolio-list__item" style="background-image: url('/local/templates/yanicode/assets/upload/iblock/526/Turanro Sp.jpg');">
        <span class="portfolio-list__title">ВАЖНО ТО, ЧТО ТЫ ЧУВСТВУЕШЬ</span>
        <span class="portfolio-list__desc">Креативная концепция и&nbsp;&nbsp;рекламная кампания </span>

            <a href="490/index.php" class="portfolio-list__item" style="background-image: url('/local/templates/yanicode/assets/upload/iblock/b71/TURANGO W TV RGB.jpg');">
        <span class="portfolio-list__title">УВЕРЕННОСТЬ, КОТОРУЮ МОЖНО КУПИТЬ</span>
        <span class="portfolio-list__desc">Креативная концепция и рекламная кампания </span>

            </a>
                    </div>
        </section>
    </main>


<template id="popup-feedback">
    <form action="/portfolio/?FILTER=48" method="post" enctype="multipart/form-data" class="js-validated-form">
        <h2 class="result"></h2>
        <div id="comp_3bcb123c6f0318b32c45337bcf616f42">
            <div class="popup-feedback__input-cover">
            <label for="" class="popup-feedback__input-label">Ваше имя</label>
            <input type="text" name="name" class="popup-feedback__input js-validated-field" data-validated_name="name">
            </div>
                <div class="popup-feedback__double-column">
            <div class="popup-feedback__input-cover">
                <label for="" class="popup-feedback__input-label">Телефон</label>
                <input type="tel" class="popup-feedback__input mask-phone-js js-validated-field" data-validated_name="phone" name="phone">
            </div>
                <div class="popup-feedback__input-cover">
                <label for="" class="popup-feedback__input-label">Email</label>
                <input type="mail" name="mail" class="popup-feedback__input js-validated-field" data-validated_name="mail">
            </div>
            </div>
            <div class="communication-format-text">
            Выберите удобный формат связи::
        </div>
        <div class="communication-format">
            <div class="communication-format__item" title="whatsapp">
                <!--                value = "34" для сохранения элементов списка в инфоблоке dev, value = "50" - prod (whatsapp)-->
                <input class="communication-format__checkbox" id="feedback-whatsapp" type="checkbox" name="way1" value="50">
                <label class="communication-format__label" for="feedback-whatsapp">
                    <svg width="49" height="51" viewbox="0 0 49 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path class="communication-format__svg" d="M24.5061 0H24.4939C10.9852 0 0 11.2168 0 25.0096C0 30.4805 1.72725 35.5512 4.66419 39.6684L1.61088 48.9594L11.0281 45.8864C14.9021 48.5061 19.5234 50.0192 24.5061 50.0192C38.0148 50.0192 49 38.7993 49 25.0096C49 11.2199 38.0148 0 24.5061 0ZM38.7621 35.3167C38.171 37.0205 35.8251 38.4335 33.9539 38.8462C32.6738 39.1244 31.0017 39.3464 25.3728 36.9642C18.1729 33.9193 13.5363 26.4508 13.1749 25.9662C12.8288 25.4817 10.2655 22.0116 10.2655 18.4227C10.2655 14.8338 12.0509 13.0863 12.7706 12.336C13.3617 11.7201 14.3386 11.4388 15.2758 11.4388C15.5789 11.4388 15.8515 11.4544 16.0965 11.4669C16.8162 11.4982 17.1776 11.5419 17.6523 12.7018C18.2433 14.1554 19.6827 17.7443 19.8542 18.1132C20.0287 18.4821 20.2033 18.9823 19.9583 19.4669C19.7286 19.9671 19.5265 20.189 19.1651 20.6142C18.8037 21.0393 18.4608 21.3645 18.0994 21.8209C17.7686 22.2179 17.395 22.6431 17.8115 23.3777C18.228 24.0968 19.6674 26.4946 21.7866 28.4203C24.5214 30.9056 26.7387 31.6997 27.5319 32.0373C28.1229 32.2874 28.8273 32.228 29.2591 31.7591C29.8073 31.1557 30.4841 30.1553 31.1732 29.1706C31.6632 28.4641 32.2818 28.3765 32.9311 28.6266C33.5926 28.8611 37.093 30.6274 37.8127 30.9932C38.5324 31.3621 39.0071 31.5371 39.1816 31.8466C39.3531 32.1561 39.3531 33.6098 38.7621 35.3167Z" fill="white"></path>
                    </svg>
                </label>
            </div>
            <div class="communication-format__item" title="skype">
<!--                value = "35" для сохранения элементов списка в инфоблоке dev, value = "51" - prod (skype)-->
                <input class="communication-format__checkbox" id="feedback-skype" type="checkbox" name="way2" value="51">
                <label class="communication-format__label" for="feedback-skype">
                    <svg width="49" height="51" viewbox="0 0 49 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path class="communication-format__svg" d="M47.5419 30.2886C51.0868 14.0299 37.1316 -0.740436 20.7431 2.09496C11.8462 -3.54044 0 2.82775 0 13.6322C0 16.1449 0.6792 18.4974 1.86015 20.525C-1.43387 36.8213 12.6356 51.3792 29.0098 48.3502C40.5847 54.5977 53.3753 42.1382 47.5419 30.2886ZM31.9632 40.0938C27.6636 41.8987 20.5881 41.9092 16.3069 39.6213C10.2023 36.2987 9.11718 28.8189 14.3835 28.8189C18.3527 28.8189 17.0962 33.5425 21.1041 35.4681C22.9439 36.3362 26.8988 36.4257 29.1974 34.8436C31.4675 33.2926 31.2595 30.8528 30.0255 29.687C26.756 26.6059 17.4002 27.83 13.123 22.4361C11.2649 20.0962 10.9141 15.968 13.1985 12.9744C17.1799 7.74493 28.8629 7.42642 33.8866 11.3756C38.5247 15.0395 37.2724 19.9026 33.5296 19.9026C29.0281 19.9026 31.3941 13.8904 24.1269 13.8904C18.8585 13.8904 16.7862 17.7146 20.4963 19.5757C25.5261 22.1259 38.3432 21.2745 38.3432 31.24C38.333 35.3869 35.8426 38.4784 31.9632 40.0938Z" fill="white"></path>
                    </svg>
                </label>
            </div>
            <div class="communication-format__item" title="zoom">
                <!--value = "36" для сохранения элементов списка в инфоблоке dev, value = "52" - prod (zoom)-->
                <input class="communication-format__checkbox" id="feedback-zoom" type="checkbox" name="way3" value="52">
                <label class="communication-format__label" for="feedback-zoom">
                    <svg width="49" height="51" viewbox="0 0 49 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path class="communication-format__svg" d="M24.5 0.980835C10.9693 0.980835 0 12.1783 0 25.9904C0 39.8025 10.9693 51 24.5 51C38.0307 51 49 39.8025 49 25.9904C49 12.1783 38.0307 0.980835 24.5 0.980835ZM30.2625 33.0569C30.261 33.2658 30.1784 33.4655 30.0329 33.6123C29.8873 33.7591 29.6907 33.841 29.4861 33.8399H14.3501C13.224 33.8443 12.1422 33.3921 11.3423 32.5828C10.5425 31.7735 10.0901 30.6732 10.0844 29.5237V18.9239C10.0859 18.7151 10.1684 18.5153 10.314 18.3686C10.4596 18.2218 10.6562 18.1399 10.8608 18.1409H25.9968C27.1229 18.1366 28.2047 18.5887 29.0045 19.398C29.8044 20.2073 30.2568 21.3076 30.2625 22.4572V33.0569ZM38.4385 33.0951C38.4385 34.0405 37.924 33.9259 37.475 33.5821L31.226 28.9316V23.0588L37.475 18.3987C38.0176 17.9404 38.4385 18.055 38.4385 18.8858V33.0951Z" fill="white"></path>
                    </svg>
                </label>
            </div>
            <div class="communication-format__item" title="viber">
<!--                value = "37" для сохранения элементов списка в инфоблоке dev, value = "53" - prod (viber)-->
                <input class="communication-format__checkbox" id="feedback-viber" type="checkbox" name="way4" value="53">
                <label class="communication-format__label" for="feedback-viber">
                    <svg width="49" height="51" viewbox="0 0 49 51" xmlns="http://www.w3.org/2000/svg">
                        <path class="communication-format__svg" d="M24.5061 0H24.4939C10.9852 0 0 11.2168 0 25.0096C0 30.4805 1.72725 35.5512 4.66419 39.6684L1.61088 48.9594L11.0281 45.8864C14.9021 48.5061 19.5234 50.0192 24.5061 50.0192C38.0148 50.0192 49 38.7993 49 25.0096C49 11.2199 38.0148 0 24.5061 0ZM38.7621 35.3167C38.171 37.0205 35.8251 38.4335 33.9539 38.8462C32.6738 39.1244 31.0017 39.3464 25.3728 36.9642C18.1729 33.9193 13.5363 26.4508 13.1749 25.9662C12.8288 25.4817 10.2655 22.0116 10.2655 18.4227C10.2655 14.8338 12.0509 13.0863 12.7706 12.336C13.3617 11.7201 14.3386 11.4388 15.2758 11.4388C15.5789 11.4388 15.8515 11.4544 16.0965 11.4669C16.8162 11.4982 17.1776 11.5419 17.6523 12.7018C18.2433 14.1554 19.6827 17.7443 19.8542 18.1132C20.0287 18.4821 20.2033 18.9823 19.9583 19.4669C19.7286 19.9671 19.5265 20.189 19.1651 20.6142C18.8037 21.0393 18.4608 21.3645 18.0994 21.8209C17.7686 22.2179 17.395 22.6431 17.8115 23.3777C18.228 24.0968 19.6674 26.4946 21.7866 28.4203C24.5214 30.9056 26.7387 31.6997 27.5319 32.0373C28.1229 32.2874 28.8273 32.228 29.2591 31.7591C29.8073 31.1557 30.4841 30.1553 31.1732 29.1706C31.6632 28.4641 32.2818 28.3765 32.9311 28.6266C33.5926 28.8611 37.093 30.6274 37.8127 30.9932C38.5324 31.3621 39.0071 31.5371 39.1816 31.8466C39.3531 32.1561 39.3531 33.6098 38.7621 35.3167Z" fill="white"></path>
                    </svg>
                </label>
            </div>
        </div>
    <div class="popup-feedback__consent">
    <div class="popup-feedback__consent-form-wrapper">
        <input class="popup-feedback__consent-input" id="consent1" type="checkbox" checked="checked">
        <label class="popup-feedback__consent-form" for="consent1">
            <a href="../data-processing/index.htm" target="_blank">
                Нажимая кнопку «Отправить», я даю свое согласие на обработку моих персональных данных, в соответствии с Федеральным законом от 27.07.2006 года №152-ФЗ «О персональных данных», на условиях и для целей, определенных в Согласии на обработку персональных данных.            </a>
        </label>
    </div>
    <div class="popup-feedback__consent-form-wrapper">
        <input class="popup-feedback__consent-input" id="consent2" type="checkbox" checked="checked">
        <label class="popup-feedback__consent-form" for="consent2">
            <a href="../privacy-policy/index.htm" target="_blank">
                Оставляя данные на Сайте, заполняя регистрационную форму, Вы соглашаетесь с настоящей Политикой конфиденциальности.            </a>
        </label>
    </div>
</div>

<div class="popup-feedback__button-cover">
    <button type="submit" class="button button_modal-gold js-button-submit"> отправить</button>
</div>
</div>    </form>
</template>

<div class="scroll-top scroll-top_show"><svg width="64" height="64" viewbox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 32h62M50 20l12 12-12 12" stroke="#c5a262" stroke-width="2"></path></svg></div>

<script src="../bitrix/templates/yanicode/assets/js/build.js"></script>
</body>
</html>

<script>
    $(document).ready(function () {

        var a = false;
        if (a === false) {
            $('body').on('submit', '.js-validated-form', function (e) {
                e.preventDefault();
                $.ajax({
                    url: "/bitrix/templates/yanicode/ajax.php",
                    type: "POST",
                    data: $(this).serialize(),
                    success: function (result) {
                        if (result==true){
                            $('.result').html('Заявка была принята');
                        }else{
                            $('.result').html('Заявка не была сохранена. Пожалуйста, введите данные еще раз.');
                        }
                    }
                });
            });
        }
    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>