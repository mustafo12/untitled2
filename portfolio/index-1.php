﻿<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$GLOBALS["arrFilterMainTheme"] = array("PROPERTY_MAIN_VALUE" => 1);
$GLOBALS["arrFilterMain"] = array("PROPERTY_MAIN_VALUE" => 1);
?>
<!doctype html>
<html xml:lang="ru" lang="ru">
<head>
    <title>Брендинг и массовые коммуникации брендов от агентства YANICODE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="index, follow">
<meta name="description" content="Брендинг и массовые коммуникации брендов от агентства YANICODE">
<script>var _ba = _ba || []; _ba.push(["aid", "2de29308e29493b74e79abe2c8129afd"]);(function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = document.location.protocol + "//bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>
<script type="text/javascript">if(!window.BX)window.BX={message:function(mess){if(typeof mess=='object') for(var i in mess) BX.message[i]=mess[i]; return true;}};</script>
<script type="text/javascript">(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','SERVER_TZ_OFFSET':'10800','SITE_ID':'s2','USER_ID':'','SERVER_TIME':'1647505455','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'2e81888949525f76737e1555de427c7f'});</script>



<script type="text/javascript">
bxSession.Expand(1440, '2e81888949525f76737e1555de427c7f', false, 'c26eccfe7fcc0bd62d50f5413b639369');
</script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div id="panel"></div>

    <main class="website-workarea">
        <section class="banner banner_before49" style=" background-image: url('/local/templates/yanicode/assets/images/banner_portfolio.jpg');">
            <div class="banner-wrapper">
                <div class="container">
                    <div class="banner__content">
                        <p>
                            Совершаем переделы рынка мирным<br>
путем инструментами брендинга от<br>
создания до продвижения на рынке.<br>
Проверить прецедентом возможно                        </p>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="stages">
                    <div class="stages__item">
                        <div class="stages__step">01</div>
                        <div class="stages__desc-step">АУДИТ</div>
                    </div>
                    <div class="stages__item">
                        <div class="stages__step">02</div>
                        <div class="stages__desc-step">СТРАТЕГИЯ</div>
                    </div>
                    <div class="stages__item">
                        <div class="stages__step">03</div>
                        <div class="stages__desc-step">КОНЦЕПЦИЯ</div>
                    </div>
                    <div class="stages__item">
                        <div class="stages__step">04</div>
                        <div class="stages__desc-step">ДИЗАЙН</div>
                    </div>
                    <div class="stages__item">
                        <div class="stages__step">05</div>
                        <div class="stages__desc-step">КОММУНИКАЦИИ</div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <h1 class="container-title" id="portfolio">
                    НАШЕ ПОРТФОЛИО                </h1>

                <div class="portfolio-filter">
                    <div class="portfolio-filter__line portfolio-filter__line_small">
                        <a class="portfolio-filter__item" href="index-1.php">Персональный брендинг</a>
                        <a class="portfolio-filter__item" href="index-2.php">Корпоративный брендинг</a>
                        <a class="portfolio-filter__item" href="index-3.php">Потребительский брендинг</a>
                        <a class="portfolio-filter__item" href="index-4.php">Ресторанный брендинг</a>
                    </div>
                    <div class="portfolio-filter__line portfolio-filter__line_middle">
                        <a class="portfolio-filter__item" href="index-5.php">Нейминг</a>
                        <a class="portfolio-filter__item" href="index-6.php">Логотип</a>
                        <a class="portfolio-filter__item" href="index-7.php">Упаковка</a>
                        <a class="portfolio-filter__item" href="index-8.php">Фирменный стиль</a>
                        <a class="portfolio-filter__item" href="index-9.php">Брендбук</a>
                    </div>
                    <div class="portfolio-filter__line portfolio-filter__line_big">
                        <a class="portfolio-filter__item" href="index-10.php">Стратегии</a>
                        <a class="portfolio-filter__item" href="index-11.php">Рекламные кампании</a>
                        <a class="portfolio-filter__item" href="index-12.php">Digital branding</a>
            </div>
                </div>
            </div>
            <div class="portfolio-list">
                                

<br>
    <a href="502/index.php" class="portfolio-list__item" style="background-image: url('/local/templates/yanicode/assets/upload/iblock/18e/1.jpg');">
        <span class="portfolio-list__title">КЁНИГСПОРТ</span>
        <span class="portfolio-list__desc">Брендинг-комплекс сети спортивных магазинов</span>

            </a>
                    </div>
        </section>
    </main>



<div class="scroll-top scroll-top_show"><svg width="64" height="64" viewbox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 32h62M50 20l12 12-12 12" stroke="#c5a262" stroke-width="2"></path></svg></div>

<script src="../bitrix/templates/yanicode/assets/js/build.js"></script>
</body>
</html>

<script>
    $(document).ready(function () {

        var a = false;
        if (a === false) {
            $('body').on('submit', '.js-validated-form', function (e) {
                e.preventDefault();
                $.ajax({
                    url: "/bitrix/templates/yanicode/ajax.php",
                    type: "POST",
                    data: $(this).serialize(),
                    success: function (result) {
                        if (result==true){
                            $('.result').html('Заявка была принята');
                        }else{
                            $('.result').html('Заявка не была сохранена. Пожалуйста, введите данные еще раз.');
                        }
                    }
                });
            });
        }
    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>